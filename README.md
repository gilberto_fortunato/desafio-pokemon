# Nome do projeto
- Desafio Pokémon.

# Descrição
- Projeto consiste em dizer o pokémon de determinada cidade de acordo com suas 
condições climáticas.

# Getting started
- Para ver o projeto funcionando basta abrir o arquivo 'index.html' e no campo
'Cidade:' informar a cidade desejada. Após isso, deverá ser precionado a tecla
'TAB' para que as informções sejam exibidas.

# Construção (Build)
- Para a construção do projeto fora necessário utilizar as seguintes tecnologias:
    * css,
    * html,
    * jquery,
    * javascript,
    * bootstrap,
    * OpenweatherMap (API),
    * PokeAPI (API)

# Features
- O comportamento do projeto se dá por meio da cidade informada pelo usuário. Conforme
a cidade vai sendo informada o projeto mostra o clima, temperatura, tipo de pokémon e 
o pokémon da mesma, além de exibir uma imagem a qual se refere ao tipo do pokémon. Vale 
lembrar que os pokémons não se repetem, mesmo se o usuário informar mais de uma vez a mesma
cidade.

# Links
- https://gitlab.com/gilberto_fortunato/desafio-pokemon.git
