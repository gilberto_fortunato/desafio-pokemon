$ (document).ready(function(){
    $("#txtCidade").focusout(function(){
        var cidade = $("#txtCidade").val();
        var keyApi = "a0c057d4ab2085492a5e68963ce6e65d"
        var urlStr = "https://api.openweathermap.org/data/2.5/weather?q="+cidade+"&appid="+keyApi+"";
        $.ajax({
            url : urlStr,
            type : "get",
            dataType : "json",
            success : function(data){

                let clima = {climas: data.weather[0].main};
                let temp = {temps: data.main.temp};

                let kelvin = temp.temps;
                let Fahrenheit = ((kelvin - 273.15) * 9/5) + 32;
                let Celsius = (Fahrenheit - 32) * 5/9;
                let CelsiusArredonda = Math.ceil(Celsius)
                
                $("#txtClima").val(clima.climas);
                $("#txtTemperatura").val(CelsiusArredonda);

                var tpPokemon = ''            

                if(CelsiusArredonda < '5' && clima.climas != 'Rain'){
                    tpPokemon = 'ice';
                }else if(CelsiusArredonda >= '5' && CelsiusArredonda < '10' && clima.climas != 'Rain'){
                    tpPokemon = 'water';
                }else if(CelsiusArredonda >= '12' && CelsiusArredonda < '15' && clima.climas != 'Rain'){
                    tpPokemon = 'grass';
                }
                else if(CelsiusArredonda >= '15' && CelsiusArredonda < '21' && clima.climas != 'Rain'){
                    tpPokemon = 'ground';
                }
                else if(CelsiusArredonda >= '23' && CelsiusArredonda < '27' && clima.climas != 'Rain'){
                    tpPokemon = 'bug';
                }
                else if(CelsiusArredonda >= '27' && CelsiusArredonda <= '33' && clima.climas != 'Rain'){
                    tpPokemon = 'rock';
                }
                else if(CelsiusArredonda >= '33' && clima.climas != 'Rain'){
                    tpPokemon = 'fire';
                }else{
                    tpPokemon = 'normal'
                }

                switch (clima.climas) {
                    case 'Rain':
                        tpPokemon = 'electric';
                        break;                
                    default: 'electric'
                        break;
                }    
                

                let tpPokemonBR = tpPokemon;

                switch (tpPokemonBR) {
                    case 'ice':
                        tpPokemonBR = 'Gelo';
                        break;
                    case 'water':
                        tpPokemonBR = 'Água';
                        break;
                    case 'grass':
                        tpPokemonBR = 'Grama';
                        break;
                    case 'ground':
                        tpPokemonBR = 'Terra';
                        break;
                    case 'bug':
                        tpPokemonBR = 'Inseto';
                        break;
                    case 'rock':
                        tpPokemonBR = 'Pedra';
                        break;
                    case 'fire':
                        tpPokemonBR = 'Fogo'; 
                        break;
                    case 'electric':
                        tpPokemonBR = 'Elétrico';
                        break;              
                    default: 'Normal'
                        break;
                }                

                $("#tpPokemon").val(tpPokemonBR);  
                
                var UrlPok = "https://pokeapi.co/api/v2/type/"+tpPokemon+""; 
                $.ajax({
                    url : UrlPok,
                    type : "get",
                    dataType : "json",
                    success : function(dadopok){    
                        
                        var count = setInterval(function(){
                            var num = parseInt(Math.random()*100)+1;
                        }, 1000);

                        console.log(count);

                        let nomepokemon = {nomespokemons: dadopok.pokemon[count].pokemon.name}
                        let StrNomePokemon = nomepokemon.nomespokemons;

                        $("#Pokemon").val(StrNomePokemon);

                    },
                    error : function(erro){
                        console.log(erro);
                    }
                });

                // chama img
                //https://github.com/HybridShivam/Pokemon/tree/master/assets/Others/type-icons
                var UrlImgTpPok = "https://raw.githubusercontent.com/HybridShivam/Pokemon/937f6143f13c2cf9ea0081cebb67a3c64d37d9c2/assets/Others/type-icons/"+tpPokemon+".svg"; 

                var newimg = $("#imgtp").attr('src', UrlImgTpPok);                                
                

            },
            error : function(erro){
                console.log(erro);
            }
        });
    });
});